#!/bin/bash

# Environment variables:
# PRIVATE_KEY — private ssh key in base64
# REMOTE_USER, REMOTE_HOST — obviously

# Read more about deployment over SSH:
# https://confluence.atlassian.com/bitbucket/access-remote-hosts-via-ssh-847452940.html

set -e  # automatic exit on error
ENV=$1

DIST_FILE="/tmp/dist.tar.gz"
REMOTE_DIR="~/www/pike-homepage"
REMOTE_DIR_OLD="$REMOTE_DIR.old"
SSH_USER="$REMOTE_USER@$REMOTE_HOST"
SSH_OPTIONS="-i /tmp/sshkey -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"

# save ssh key to file
echo $PRIVATE_KEY | base64 --decode -i > /tmp/sshkey
chmod 600 /tmp/sshkey

# cretae tar.gz archive and upload it to server
tar -czf $DIST_FILE --exclude .git --exclude dist.tar.gz ./
scp $SSH_OPTIONS $DIST_FILE $SSH_USER:~/dist.tar.gz

# unpack archive on server and replace old version
ssh $SSH_USER $SSH_OPTIONS << EOF
    mkdir -p $REMOTE_DIR
    mkdir -p ~/www/dist
    tar xf ~/dist.tar.gz -C ~/www/dist

    if [ -d $REMOTE_DIR/dist ]; then
        rm -rf $REMOTE_DIR/dist
    fi

    if [ -d $REMOTE_DIR_OLD ]; then
        rm -rf $REMOTE_DIR_OLD
    fi
    mv $REMOTE_DIR $REMOTE_DIR_OLD
    mv ~/www/dist $REMOTE_DIR
EOF

rm /tmp/sshkey
rm $DIST_FILE
echo "Done"
exit 0
