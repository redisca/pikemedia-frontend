import {TweenMax, TimelineMax, Power0, Linear} from "gsap/TweenMax";
import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators'
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap'
//
window.addEventListener('DOMContentLoaded', () => {
    // const controller = new ScrollMagic.Controller();
    const controller = window.sceneController;

    const tween = new TweenMax.fromTo('.service-item_archive__rock-feature .rock-feature__circle', 1, {
        rotation: -30,
        ease: Linear.easeNone
    }, {
        rotation: 30
    });

    new ScrollMagic.Scene({
        triggerElement: '.service-item_archive__rock-feature',
        duration: '120%',
        triggerHook: 1
    }).setTween(tween)
        .addTo(controller);


    const tween2 = new TweenMax.fromTo('.service-item_promo__rock-feature .rock-feature__circle', 1, {
        rotation: -30,
        ease: Linear.easeNone
    }, {
        rotation: 30
    });

    const a = new ScrollMagic.Scene({
        triggerElement: '.service-item_promo__rock-feature',
        duration: '120%',
        triggerHook: 1
    }).setTween(tween2).addTo(controller);
});