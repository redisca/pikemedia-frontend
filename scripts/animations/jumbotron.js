import {TweenMax} from "gsap/TweenMax";
import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap'

window.addEventListener('DOMContentLoaded', () => {
    const controller = window.sceneController;

    new ScrollMagic.Scene({
        triggerElement: '.section_header',
        reverse: false,
        duration: '100%'

    }).setClassToggle('body', 'header-awaiting-animation').addTo(controller);

    new ScrollMagic.Scene({
        triggerElement: '.section_header',
        reverse: false,
        duration: '100%'

    }).setClassToggle('body', 'animate-jumbotron').addTo(controller);

    new ScrollMagic.Scene({
        reverse: false,
    }).setClassToggle('.curtain', 'animate').addTo(controller);

    const curtain = document.querySelector('.curtain');
    curtain.addEventListener('transitionend', () => {
        curtain.remove();
    });


    const navigationText = document.querySelector('.navigation__text');
    const transitionHandler = () => {
        setTimeout(() => {
            document.body.classList.remove('header-awaiting-animation', 'animate-jumbotron');
            navigationText.removeEventListener('transitionend', transitionHandler);
        }, 200);

        // on scroll animations
        const scrollTimeline = new TimelineMax();
        const line1ScrollTween = new TweenMax.fromTo('.jumbotron__logo div:nth-child(1)', 1, {
                transform: `translate3d(0, 0, 0)`

            },
            {
                transform: `translate3d(120%, 5vh, 0)`
            });
        const line2ScrollTween = new TweenMax.fromTo('.jumbotron__logo div:nth-child(2)', 1, {
            transform: `translate3d(0, 0, 0)`

        }, {
            transform: 'translate3d(-120%, 5vh, 0)'
        });

        const backgroundScrollTween = new TweenMax.to('.jumbotron__background', 1, {
            opacity: 0
        });

        scrollTimeline
            .add("start")
            .add(line1ScrollTween, "start")
            .add(line2ScrollTween, "start");

        new ScrollMagic.Scene({
            duration: window.innerHeight
        })
            .setTween(scrollTimeline)
            .addTo(controller);

        new ScrollMagic.Scene({
            duration: window.innerHeight * 0.4
        }).setTween(backgroundScrollTween).addTo(controller);
    };

    navigationText.addEventListener('transitionend', transitionHandler);

});