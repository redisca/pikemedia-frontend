import {TweenMax, TimelineMax, Power1} from "gsap/TweenMax";
import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap'

window.addEventListener('DOMContentLoaded', () => {
    const controller = new ScrollMagic.Controller();

    new ScrollMagic.Scene({
        triggerElement: '.section_header',
        offset: window.innerHeight * 0.6
    }).setClassToggle('.navigation', 'navigation_collapsed').addTo(controller);
});