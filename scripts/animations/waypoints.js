import {TweenMax, TimelineMax, Power1} from "gsap/TweenMax";
import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap'

import {mediaHubAnimation, signalAnimation, archiveAnimation, promoAnimation} from "../elements/animated-svg";

import animateNumber from '../elements/numbers';

//
window.addEventListener('DOMContentLoaded', () => {
    const controller = new ScrollMagic.Controller();

    new ScrollMagic.Scene({
        triggerElement: '.clients-section',
        triggerHook: 1,
        reverse: false,
        offset: 100,
    }).setClassToggle('.statistics', 'animate').on("enter", (event) => {
        if (event.scrollDirection === 'FORWARD') {
            document.querySelectorAll('.statistics-item__value').forEach((element, index) => {
                animateNumber(element, 200 * index, 1200);
            })
        }
    }).addTo(window.sceneController);

    new ScrollMagic.Scene({
        triggerElement: '.clients',
        triggerHook: 1,
        reverse: false,
        // offset: 100,
    }).setClassToggle('.clients', 'animate').addTo(controller);

    // change bg
    new ScrollMagic.Scene(({
        triggerHook: 0,
        triggerElement: '.clients__header',
    })).setClassToggle('#section-bg', 'section_background_hidden').addTo(controller);

    // services sections

    new ScrollMagic.Scene(({
        triggerElement: '.service-item_mediahub',
        triggerHook: 1,
        offset: 100,
        reverse: false
    })).setClassToggle('.service-item_mediahub', 'animate').on('enter', () => {
        mediaHubAnimation.play();
    }).addTo(controller);

    new ScrollMagic.Scene(({
        triggerElement: '.service-item_archive',
        triggerHook: 1,
        offset: 100,
        reverse: false
    })).setClassToggle('.service-item_archive', 'animate').on('enter', () => {
        archiveAnimation.play();
    }).addTo(controller);

    new ScrollMagic.Scene(({
        triggerElement: '.service-item_promo',
        triggerHook: 1,
        offset: 100,
        reverse: false
    })).setClassToggle('.service-item_promo', 'animate').on('enter', () => {
        promoAnimation.play();
    }).addTo(controller);

    new ScrollMagic.Scene(({
        triggerElement: '.service-item_signal',
        triggerHook: 1,
        offset: 100,
        reverse: false
    })).setClassToggle('.service-item_signal', 'animate').on('enter', () => {
        signalAnimation.play();
    }).addTo(controller);

    // pml live
    new ScrollMagic.Scene(({
        triggerElement: '.pml-live',
        triggerHook: 1,
        offset: document.querySelector('.pml-live').offsetHeight * 0.3
    })).setClassToggle('.pml-live', 'animate').addTo(controller);

    new ScrollMagic.Scene(({
        triggerElement: '.pml-live',
        triggerHook: 0,
        offset: document.querySelector('.pml-live').offsetHeight * 0.3,
    })).setClassToggle('.pml-live', 'animate-next').addTo(controller);

    // integration
    new ScrollMagic.Scene(({
        triggerElement: '.integration',
        triggerHook: 0.5,
        reverse: false
    })).setClassToggle('.integration', 'animate').addTo(controller);


    // experience
    new ScrollMagic.Scene({
        triggerElement: '.experience__container',
        triggerHook: 1,
        reverse: false,
        // offset: 100,
    }).setClassToggle('.experience', 'animate').addTo(controller);

    new ScrollMagic.Scene({
        triggerElement: '.contact-section-main .container',
        triggerHook: 1,
        reverse: false,
        // offset: 100,
    }).setClassToggle('.contact-section-main', 'animate').addTo(controller);

});