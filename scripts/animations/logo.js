import {TweenMax, TimelineMax, Power1} from "gsap/TweenMax";
import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap'


window.addEventListener('DOMContentLoaded', () => {
    const controller = new ScrollMagic.Controller();
    const logoHeight = document.querySelector('.logo').offsetHeight;


    new ScrollMagic.Scene({
        triggerHook: 0,
        triggerElement: '.clients__header',
        duration:
            document.querySelector('.pml-live').getBoundingClientRect().top
            - document.querySelector('.clients__header').getBoundingClientRect().top
            - logoHeight * 0.8
    }).setClassToggle('.logo', 'logo_inversed').addTo(controller);


    new ScrollMagic.Scene({
        triggerElement: '.integration-section',
        triggerHook: 0,
        offset: -logoHeight * 0.9,
        duration: document.querySelector('.integration-section').offsetHeight - logoHeight * 0.6
    }).setClassToggle('.logo', 'logo_inversed').addTo(controller);

});