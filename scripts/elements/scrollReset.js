window.addEventListener('beforeunload', () => {
    if (window.pageYOffset > 0 && window.pageYOffset < window.innerHeight) {
        window.scrollTo(0, 0);
    }
});