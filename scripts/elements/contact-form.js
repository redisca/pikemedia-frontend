import IMask from 'imask';
import autosize from 'autosize';
import axios from 'axios';
import EmailValidator from 'email-validator';

window.addEventListener('DOMContentLoaded', () => {
    const forms = document.querySelectorAll('.contacts__form');

    const validate = (form) => {
        const nameInput = form.querySelector('[name=name]');
        const phoneInput = form.querySelector('[name=phone]');
        const messageInput = form.querySelector('[name=message]');
        const emailInput = form.querySelector('[name=email]');
        let error = false;

        if (nameInput.value === '') {
            nameInput.classList.add('input_error');
            nameInput.nextElementSibling.innerText = 'заполните поле';
            error = true;
        } else {
            nameInput.classList.remove('input_error');
            nameInput.nextElementSibling.innerText = '';
        }

        if (phoneInput.value === '') {
            phoneInput.classList.add('input_error');
            phoneInput.nextElementSibling.innerText = 'заполните поле';
            error = true;
        } else if (phoneInput.value.length !== 16) {
            phoneInput.classList.add('input_error');
            phoneInput.nextElementSibling.innerText = 'неправильный формат';
            error = true;
        } else {
            phoneInput.classList.remove('input_error');
            phoneInput.nextElementSibling.innerText = '';
        }

        if (messageInput.value === '') {
            messageInput.classList.add('input_error');
            messageInput.nextElementSibling.innerText = 'заполните поле';
            error = true;
        } else {
            messageInput.classList.remove('input_error');
            messageInput.nextElementSibling.innerText = '';
        }

        if (emailInput.value === '') {
            emailInput.classList.add('input_error');
            emailInput.nextElementSibling.innerText = 'заполните поле';
        } else if (!EmailValidator.validate(emailInput.value)) {
            emailInput.classList.add('input_error');
            emailInput.nextElementSibling.innerText = 'неправильный формат';
        } else {
            emailInput.classList.remove('input_error');
            emailInput.nextElementSibling.innerText = '';
        }

        return !error;
    };

    const clearFields = (fields) => {
        fields.forEach(field => {
            field.value = '';
        })
    };

    forms.forEach(form => {
        const fields = [form.querySelector('[name=name]'),
            form.querySelector('[name=phone]'),
            form.querySelector('[name=message]'),
            form.querySelector('[name=email]')];
        const button = form.querySelector('.button');

        IMask(form.querySelector('[name=phone]'), {
            mask: '+{7} 000 000-00-00'
        });

        autosize(form.querySelector('[name=message]'));

        fields.forEach(field => {
            field.addEventListener('keydown', () => {
                field.classList.remove('input_error');
            })
        });

        form.addEventListener('submit', (event) => {
            button.setAttribute('disabled', 'true');

            if (!validate(form)) {
                event.preventDefault();
                button.removeAttribute('disabled');
                return;
            }

            axios.get('https://api.pikemedia.ru/send', {
                params: {
                    name: fields[0].value,
                    phone: fields[1].value,
                    message: fields[2].value,
                    email: fields[3].value
                }
            }).then(response => {
                clearFields(fields);
                form.parentElement.classList.add('contact-section_success');
                button.removeAttribute('disabled');
            }).catch((err) => {
                alert('К сожалению, возникла ошибка. Попробуйте снова или свяжитесь с нами, используя контактные данные, указанные на сайте');
                button.removeAttribute('disabled');
                console.log(err);
            });

            event.preventDefault();
        })
    });
});
