import {easeOutQuad} from "js-easing-functions";

export default (element, delay, duration) => {
    const endValue = parseInt(element.innerText, 10);
    element.innerText = '0';
    let start = null;

    const animate = (timestamp) => {
        if (!start) {
            start = timestamp;
        }

        const progress = (timestamp - start);

        if (progress <= duration) {
            element.innerText = Math.round(easeOutQuad(progress, 0, endValue, duration)).toLocaleString();

            window.requestAnimationFrame(animate);
        } else {
            element.innerText = endValue.toLocaleString();
        }
    };

    setTimeout(() => window.requestAnimationFrame(animate), delay);
};