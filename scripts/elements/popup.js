import {TweenMax} from "gsap/TweenMax";
import { disablePageScroll, enablePageScroll } from 'scroll-lock';

window.addEventListener('DOMContentLoaded', () => {
    const popupTriggers = document.querySelectorAll('[data-popup]');

    popupTriggers.forEach((element) => {
        const target = element.getAttribute('data-popup');
        const targetElement = document.querySelector(target);

        if (!targetElement) {
            return;
        }

        element.addEventListener('click', () => {
            targetElement.classList.add('popup_shown');
            TweenMax.fromTo(targetElement, 0.4, {opacity: 0}, {opacity: 1});
            disablePageScroll(targetElement);
        });

        targetElement.querySelector('.popup__close').addEventListener('click', () => {
            TweenMax.fromTo(targetElement, 0.4, {opacity: 1}, {
                opacity: 0,
                onComplete: () => {
                    targetElement.classList.remove('popup_shown');
                }
            });
            enablePageScroll(targetElement);
        });
    })
});