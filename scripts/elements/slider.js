import {tns} from "../../node_modules/tiny-slider/src/tiny-slider";
import {detect} from "detect-browser";

window.addEventListener('DOMContentLoaded', () => {
    const browserName = detect().name;
    const isMicrosoftBrowser = browserName === 'ie' || browserName === 'edge';

    tns({
        container: '.clients .slider',
        prevButton: '.clients .slider__control_left',
        nextButton: '.clients .slider__control_right',
        loop: false,
        autoWidth: isMicrosoftBrowser,
        nav: false,
        speed: 500,
        preventScrollOnTouch: 'auto',
        touch: true,
        swipeAngle: 35,
        responsive: {
            768: {
                items: 2,
                fixedWidth: 407,
            },
            1024: {
                items: 3,
                fixedWidth: 407,
            },
            1440: {
                items: 3,
                fixedWidth: 500,
            }
        }
    });

    tns({
        container: '.experience .slider',
        prevButton: '.experience .slider__control_left',
        nextButton: '.experience .slider__control_right',
        autoWidth: isMicrosoftBrowser,
        loop: false,
        nav: false,
        speed: 500,
        preventScrollOnTouch: 'auto',
        touch: true,
        swipeAngle: 35,
        responsive: {
            768: {

                items: 2,
                fixedWidth: 425,
            },
            1024: {
                items: 3,
                fixedWidth: 425,
            },
            1440: {
                items: 3,
                fixedWidth: 500,
            }
        }
    });
});