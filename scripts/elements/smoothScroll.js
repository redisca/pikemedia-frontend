import Scrollbar from 'smooth-scrollbar';
import ScrollMagic from 'scrollmagic';

if (window.innerWidth >= 1024) {
    window.sceneController = new ScrollMagic.Controller({container: '.content'});
} else {
    window.sceneController = new ScrollMagic.Controller();

}
const controller = window.sceneController;

if (window.innerWidth >= 1024) {
    const instance = Scrollbar.init(document.querySelector('.content'), {
        speed: 0.5
    });

    controller.scrollPos(function () {
        return instance.offset.y;
    });

    instance.addListener(function () {
        controller.update();
    });
}