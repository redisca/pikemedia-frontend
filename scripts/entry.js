import "core-js/stable";

import {TweenMax} from 'gsap/TweenMax'
import ScrollMagic from 'scrollmagic'
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap'

import './libs/keyshape.min'

import './elements/smoothScroll'


import './animations/jumbotron'
import './animations/navbar'
import './animations/waypoints'
import './animations/rock-feature'
import './animations/logo'

import './elements/slider'
import './elements/numbers'
import './elements/popup'
import './elements/contact-form'
